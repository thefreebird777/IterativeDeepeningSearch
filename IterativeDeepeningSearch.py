def RecursiveDLS(node, limit, goal):
    global stack
    #each time a node is visted it is added to the stack
    stack.push(node)
    #checks if node is the goal
    if node.data == goal:
        return 'solution'
    elif limit == 0:
        #each time there is a cutoff a node is popped from the stack
        stack.pop()
        return 'cutoff'
    else:
        cutoff_occurred = False
        #loops through each child of the current node
        for child in node.children:
            #recursive call to this function with the child as the new node
            result = (RecursiveDLS(child, limit - 1, goal))
            if result == 'cutoff':
                cutoff_occurred = True
            elif result != 'failure':
                  return result
        if cutoff_occurred:
            # each time there is a cutoff a node is popped from the stack
            stack.pop()
            return 'cutoff'
        else:
            return 'failure'

def DepthLimitedSearch(tree, limit, goal):
    return RecursiveDLS(tree, limit, goal)


def IterativeDeepeningSearch(tree, goal):
    depth = 0
    while True:
        result = DepthLimitedSearch(tree, depth, goal)
        if result != 'cutoff':
            return result
        depth += 1 #whenever there is a cutoff and the current depth is exhausted, then the depth limit needs to be increased

#basic stack class
class Stack():
    def __init__(self):
        self.items = []

    def isEmpty(self):
        return self.items == []

    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def peek(self):
        return self.items[len(self.items) - 1]

    def size(self):
        return len(self.items)

# tree class for generating tree to test the IDS algo
class Tree(object):
    def __init__(self, data='root', children=None):
        self.data = data
        self.children = []
        if children is not None:
            for child in children:
                self.add_child(child)

    def __repr__(self):
        return self.data

    def add_child(self, node):
        assert isinstance(node, Tree)
        self.children.append(node)


if __name__ == '__main__':
    #global stack variable will contain the path the algorithm finds
    global stack
    stack = Stack()

    # Populates test tree as:
    #     A
    #   /   \
    #  B     C
    # / \    / \
    # D  E   F  G
    # /\  /\  /\  /\
    # H I J K L M N O
    tree = Tree('A', [Tree('B', [Tree('D', [Tree('H'), Tree('I')]), Tree('E', [Tree('J'), Tree('K')])]),
                      Tree('C', [Tree('F', [Tree('G'), Tree('L')]), Tree('M', [Tree('N'), Tree('O')])])])

    GOAL = raw_input("Please enter the alphabetic letter (A-O) that you would like to search for: ")
    GOAL = GOAL.upper()

    if GOAL != 'A' and GOAL != 'B' and GOAL != 'C' and GOAL != 'D' and GOAL != 'E' and GOAL != 'F' and GOAL != 'G' and GOAL != 'H' and GOAL != 'I' and GOAL != 'J' and GOAL != 'K' and GOAL != 'L' and GOAL != 'M' and GOAL != 'N' and GOAL != 'O':
        print "Invalid input"
    else:
        r = IterativeDeepeningSearch(tree, GOAL)
        if r == 'solution':
            print "Path to " + GOAL + " is: "
            print stack.items
        elif r == 'failure':
            print "Failed to find a path."
